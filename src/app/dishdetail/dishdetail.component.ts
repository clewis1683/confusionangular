// Original (Form validation) : import { Component, OnInit } from '@angular/core';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';
import { visibility, flyInOut, expand } from '../animations/app.animation';
import { FavoriteService } from '../services/favorite.service';

// Aditionals
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comment } from '../shared/comment';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    }
})
export class DishDetailComponent implements OnInit {

  dish: Dish;
  errMess: string;
  dishIds: string[];
  prev: string;
  next: string;
  visibility = 'shown';
  favorite = false;

// Aditionals (Form validation)
commentForm: FormGroup;
commentObject: Comment;
@ViewChild('fform') commentFormDirective;

dishcopy: Dish;

// Aditionals: 

formErrors = {
  'author': '',
  'comment': ''
};

validationMessages = {
  'author': {
    'required': 'Name is required.',
    'minlength': 'Name must be at least 2 characters long',
    'maxlength': 'Name cannot be more than 25 characterts long'
  },
  'comment': {
    'required': 'Comment is required.'
  }
};

/* Original:  constructor(private dishService: DishService,
    private route: ActivatedRoute,
    private location: Location) { }*/

  constructor(private dishService: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    private favoriteService: FavoriteService,
    @Inject('BaseURL') private BaseURL) {}

  ngOnInit(): void {
    this.createForm();
    this.dishService.getDishIds()
      .subscribe((dishIds) => this.dishIds = dishIds);
    this.route.params
      .pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(params['_id']); }))
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish._id); this.visibility = 'shown'; 
        this.favoriteService.isFavorite(this.dish._id)
          .subscribe(resp => { console.log(resp); this.favorite = <boolean>resp.exists; },
            err => console.log(err));},
        errmess => this.errMess = <any>errmess);
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

// Form validation: 

createForm(): void {
  this.commentForm = this.fb.group({
    rating: 5,
    comment: ['', [Validators.required]],
    author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
    date: ''
  });

  this.commentForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
  
  this.onValueChanged(); // (re)set form validation messages
}

onValueChanged(data?: any) {
  if (!this.commentForm) { return; }
  const form = this.commentForm;
  for (const field in this.formErrors) {
    if (this.formErrors.hasOwnProperty(field)) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          if (control.errors.hasOwnProperty(key)) {
            this.formErrors[field] += messages[key] + ' ';
          }
        }
      }
    }
  }
}

onSubmit(): void {
  this.commentForm.value.date = (new Date()).toISOString();
  this.commentObject = this.commentForm.value;
  this.dishcopy.comments.push(this.commentObject);
  this.dishService.putDish(this.dishcopy)
      .subscribe(dish => {
        this.dish = dish; this.dishcopy = dish;
      },
      errmess => { this.dish = null; this.dishcopy = null; this.errMess = <any>errmess; });
  console.log(this.commentObject);
  this.commentFormDirective.resetForm();
  this.commentForm.reset({
    rating: 5,
    comment: '',
    author: '',
    date: ''
  });
}

addToFavorites() {
  if (!this.favorite) {
    this.favoriteService.postFavorite(this.dish._id)
      .subscribe(favorites => { console.log(favorites); this.favorite = true; });
  }
}
}
